# Repozytorium informacji o modułach

# Jak generowac changelogi

`conventional-changelog -r 10 -o CHANGELOG.md`
Ogólnie, taka komenda 10 ile ostanich wersji ma objac, -o do jakiego pliku zapisac

## Struktura 

	{ISO} - KOD ISO alfa-2 https://pl.wikipedia.org/wiki/ISO_3166-1_alfa-2
	- modulename
	|- changelog.txt
	|- changelog_pl.shtml
	|- changelog_{ISO}.shtml
	|- description_short.shtml
	|- description_short_{ISO}.shtml

## Opis struktury

`*.shtml` Smarty HTML

`changelog.txt` - Zawiera changelog (zakladamy, ze jak nie mamy ang. opisu, to robimy tutaj PL, jak mamy ANG, to PL jako overwrite)  
`changelog_pl.txt` - zawiera nadpisanie dla jezyka angielskiego  
`changelog_{ISO}.txt` - zawiera nadpisanie dla jezyka oznaczonego kodem {ISO}  
`description_short.txt` - Krótki opis  
`description_short_{ISO}.txt` - zawiera nadpisanie dla jezyka oznaczonego kodem {ISO}

! Ważna poprawka
+ Nowa funkcja
- Informacja o zmianie
# Fix
